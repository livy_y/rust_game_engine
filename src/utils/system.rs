use time::{Duration, Instant};

/// Small timer
pub struct Timer {
    last_tick: Duration,
    instant: Instant,
}

impl Timer {
    /// Start the timer
    pub fn new() -> Self {
        Timer {
            last_tick: Duration::ZERO,
            instant: Instant::now(),
        }
    }

    /// Gets the amount of time past between this function call and the previous function call or
    /// the start of the timer.
    pub fn delta(&mut self) -> i128 {
        let tick = self.instant.elapsed();
        let delta = tick - self.last_tick;
        self.last_tick = tick;

        delta.whole_nanoseconds()
    }

    /// Reset the timer from 0. This is the same as making a new timer.
    pub fn reset(&mut self) {
        self.last_tick = self.instant.elapsed();
    }
}

