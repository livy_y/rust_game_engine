use winit;
use winit::event::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::platform::run_return::EventLoopExtRunReturn;

use crate::graphics::input::{InputMap, KeyKeyboard};

/// Types of events the program can have
pub enum Event {
    Resize,
    Close,
    Continue
}

/// Handles and controls the input
pub struct InputHandler {
    events_loop: EventLoop<()>,
    input_map: InputMap
}

impl InputHandler {
    /// Inits InputHandler
    pub fn new(events_loop: EventLoop<()>) -> Self {
        InputHandler {
            events_loop,
            input_map: InputMap::new()
        }
    }

    /// Gets a reference of the input map
    pub fn get_input_map(&self) -> &InputMap {
        &self.input_map
    }

    /// Gets the Event from the main loop
    pub fn read_inputs(&mut self) -> Event {
        let mut status = Event::Continue;

        let input_map = &mut self.input_map;

        input_map.reset();

        self.events_loop.run_return(|event, _, control_flow| {
            *control_flow = ControlFlow::Exit;
            if let winit::event::Event::WindowEvent { event, .. } = event { match event {
                WindowEvent::CloseRequested => status = Event::Close,
                WindowEvent::Resized(_) => status = Event::Resize,
                WindowEvent::KeyboardInput {
                    input: KeyboardInput {
                        virtual_keycode: Some(key_code),
                        state, ..
                    }, ..
                } => match key_code {
                    VirtualKeyCode::Escape => status = Event::Close,
                    _ => {
                        let key = Self::get_input_key(key_code);
                        match state {
                            ElementState::Pressed => input_map.key_press(key),
                            ElementState::Released => input_map.key_release(key)
                        }
                    },
                },
                _ => ()
            } }
        });

        status
    }

    /// Turns Winit keys into Tulpen keys
    fn get_input_key(key_code: VirtualKeyCode) -> KeyKeyboard {
        match key_code {
            VirtualKeyCode::Left => KeyKeyboard::Left,
            VirtualKeyCode::Right => KeyKeyboard::Right,
            VirtualKeyCode::Up => KeyKeyboard::Up,
            VirtualKeyCode::Down => KeyKeyboard::Down,
            VirtualKeyCode::PageUp => KeyKeyboard::PageUp,
            VirtualKeyCode::PageDown => KeyKeyboard::PageDown,
            VirtualKeyCode::Return => KeyKeyboard::Return,
            VirtualKeyCode::Space => KeyKeyboard::Space,
            VirtualKeyCode::Tab => KeyKeyboard::Tab,
            VirtualKeyCode::A => KeyKeyboard::A,
            VirtualKeyCode::B => KeyKeyboard::B,
            VirtualKeyCode::C => KeyKeyboard::C,
            VirtualKeyCode::D => KeyKeyboard::D,
            VirtualKeyCode::E => KeyKeyboard::E,
            VirtualKeyCode::F => KeyKeyboard::F,
            VirtualKeyCode::G => KeyKeyboard::G,
            VirtualKeyCode::H => KeyKeyboard::H,
            VirtualKeyCode::I => KeyKeyboard::I,
            VirtualKeyCode::J => KeyKeyboard::J,
            VirtualKeyCode::K => KeyKeyboard::K,
            VirtualKeyCode::L => KeyKeyboard::L,
            VirtualKeyCode::M => KeyKeyboard::M,
            VirtualKeyCode::N => KeyKeyboard::N,
            VirtualKeyCode::O => KeyKeyboard::O,
            VirtualKeyCode::P => KeyKeyboard::P,
            VirtualKeyCode::Q => KeyKeyboard::Q,
            VirtualKeyCode::R => KeyKeyboard::R,
            VirtualKeyCode::S => KeyKeyboard::S,
            VirtualKeyCode::T => KeyKeyboard::T,
            VirtualKeyCode::U => KeyKeyboard::U,
            VirtualKeyCode::V => KeyKeyboard::V,
            VirtualKeyCode::W => KeyKeyboard::W,
            VirtualKeyCode::X => KeyKeyboard::X,
            VirtualKeyCode::Y => KeyKeyboard::Y,
            VirtualKeyCode::Z => KeyKeyboard::Z,
            _ => KeyKeyboard::Unknown
        }
    }
}