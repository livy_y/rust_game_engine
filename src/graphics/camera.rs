use cgmath::{Deg, Matrix4, SquareMatrix, Point3, Vector3, perspective, ortho};

pub trait Camera {
    fn update(&mut self);
    fn matrix(&self) -> Matrix4<f32>;
    fn get_position(&self) -> Point3<f32>;
    fn set_position(&mut self, position: Point3<f32>); 
    fn move_camera(&mut self, x: f32, y: f32, z: f32);
}

pub struct PerspectiveCamera {
    position: Point3<f32>,
    projection: Matrix4<f32>,
    view: Matrix4<f32>,
    width: f32,
    height: f32,
    aspect: f32,
    fov: f32,
    near: f32,
    far: f32
}

impl Camera for PerspectiveCamera {
    fn update(&mut self) {
        self.projection = Self::perspective_projection(self.fov, self.aspect, self.near, self.far);
    }

    fn matrix(&self) -> Matrix4<f32> {
        self.projection * self.view
    }

    fn get_position(&self) -> Point3<f32> {
        self.position
    }

    fn set_position(&mut self, position: Point3<f32>) {
        self.position = position;
    }

    fn move_camera(&mut self, x: f32, y: f32, z: f32) {
        let original_pos = self.get_position();
        self.position = Point3::new(x + original_pos.x, y + original_pos.y, z + original_pos.z);
    }
}

impl PerspectiveCamera {
    pub fn new(near: f32, far: f32, fov: f32, aspect: f32) -> Self {
        let correction = Self::correction();

        let projection =
            correction * perspective(Deg(fov), aspect, near, far);

        Self {
            position: [0., 0., 0.].into(),
            projection,
            view: Matrix4::identity(),
            width: 1.,
            height: 1.,
            aspect,
            fov,
            near,
            far
        }
    }

    pub fn set_aspect(&mut self, aspect: f32) {
        self.aspect = aspect;

        self.update();
    }

    pub fn resize(&mut self, width: f32, height: f32) {
        self.width = width;
        self.height = height;

        self.update();
    }

    fn perspective_projection(fov: f32, aspect: f32, near: f32, far: f32) -> Matrix4<f32> {
        Self::correction() * perspective(Deg(fov), aspect, near, far)
    }

    fn correction() -> Matrix4<f32> {
        // api use a different coord system than opengl
        // the ortho matrix needs to be corrected
        let mut correction = Matrix4::identity();
        correction.y.y = -1.0;
        correction.z.z = 0.5;
        correction.w.z = 0.5;

        correction
    }

    pub fn transform(&mut self, transform: Matrix4<f32>) {
        self.view = transform * self.view
    }

    pub fn look_at<V: Into<Vector3<f32>>>(&mut self, direction: V, up: V) {
        self.view = Matrix4::look_to_rh(
            self.position, direction.into(), up.into());
    }
}

pub struct OrthoCamera {
    position: Point3<f32>,
    projection: Matrix4<f32>,
    view: Matrix4<f32>,
    width: f32,
    height: f32,
    aspect: f32,
    fov: f32,
    near: f32,
    far: f32
}


impl Camera for OrthoCamera {
    fn update(&mut self) {
        self.projection = Self::ortho_projection(self.width, self.height, self.near, self.far);
    }

    fn matrix(&self) -> Matrix4<f32> {
        self.projection * self.view
    }

    fn get_position(&self) -> Point3<f32> {
        self.position
    }

    fn set_position(&mut self, position: Point3<f32>) {
        self.position = position;
    }

    fn move_camera(&mut self, x: f32, y: f32, z: f32) {
        let original_pos = self.get_position();
        self.position = Point3::new(x + original_pos.x, y + original_pos.y, z + original_pos.z);
    }
}

impl OrthoCamera {
    pub fn new(width: f32, height: f32) -> Self {
        let near = -10.;
        let far = 10.;

        let projection = Self::ortho_projection(width, height, near, far);

        Self {
            position: [0., 0., 0.].into(),
            projection,
            view: Matrix4::identity(),
            width,
            height,
            aspect: 1.,
            fov: 60.,
            near,
            far
        }
    }

    pub fn set_aspect(&mut self, aspect: f32) {
        self.aspect = aspect;

        self.update();
    }

    pub fn resize(&mut self, width: f32, height: f32) {
        self.width = width;
        self.height = height;

        self.update();
    }

    fn ortho_projection(width: f32, height: f32, near: f32, far: f32) -> Matrix4<f32> {
        let left = -width / 2.;
        let right = width / 2.;
        let top = height / 2.;
        let bottom = -height / 2.;

        Self::correction() * ortho(
            left, right, bottom, top, near, far
        )
    }

    fn correction() -> Matrix4<f32> {
        // api use a different coord system than opengl
        // the ortho matrix needs to be corrected
        let mut correction = Matrix4::identity();
        correction.y.y = -1.0;
        correction.z.z = 0.5;
        correction.w.z = 0.5;

        correction
    }

    pub fn transform(&mut self, transform: Matrix4<f32>) {
        self.view = transform * self.view
    }

    pub fn look_at<V: Into<Vector3<f32>>>(&mut self, direction: V, up: V) {
        self.view = Matrix4::look_to_rh(
            self.position, direction.into(), up.into());
    }
}

pub fn matrix_to_string(matrix: [[f32; 4]; 4]) -> String{
    let mut result: String = "[".into();

    for x in matrix {
        result.push_str("[");
        for y in x {
            result.push_str(y.to_string().as_str());
            result.push_str(", ");
        }
        result.push_str("]\n")
    }
    result.push_str("]") ;
    result
}
