#[allow(unused_imports)]
use glium::{glutin, Surface};
use glium::Display;
use glium::index::PrimitiveType;
use winit::dpi::{PhysicalPosition, Position};
use winit::event_loop::EventLoop;

use crate::graphics::renderer::{Renderer, Vertex};
use crate::graphics::window::{WindowMode, WindowOptions};
use crate::AppCreateInfo;
use crate::scene::ObjectManager;
use crate::graphics::camera::matrix_to_string;


/// This handles the GLWindow for the OpenGL API
pub struct GLWindow {
    /// The glium target display
    display: Display,
}

impl GLWindow {
    /// Create a new window from the WindowOptions enum
    pub(crate) fn new(opts: WindowOptions) -> (Self, EventLoop<()>) {
        let event_loop = glutin::event_loop::EventLoop::new();
        let wb = glutin::window::WindowBuilder::new()
            //.with_inner_size(Size::Physical(PhysicalSize::new(opts.window_start_width, opts.window_start_height)))
            //.with_min_inner_size(Size::Physical(PhysicalSize::new(opts.window_min_width.unwrap(), opts.window_min_height.unwrap())))
            //.with_max_inner_size(Size::Physical(PhysicalSize::new(opts.window_max_width.unwrap(), opts.window_max_height.unwrap())))
            .with_position(Position::Physical(PhysicalPosition::new(opts.window_start_position_x, opts.window_start_position_y)))
            //.with_resizable(opts.resizable)
            .with_title(opts.window_title)
            .with_maximized(opts.maximized)
            .with_decorations(opts.decorations)
            .with_always_on_top(opts.always_on_top);

        match opts.window_mode {
            WindowMode::Fullscreen => { /* TODO: add fullscreen support here */ }
            WindowMode::BorderlessFullscreen => { /* TODO: add borderless fullscreen support here */ }
            WindowMode::Windowed => {}
        }

        let cb = glutin::ContextBuilder::new(); //TODO: look at these options
        let display = glium::Display::new(wb, cb, &event_loop).unwrap();

        (Self {
            display,
        },
         event_loop)
    }
}

/// OpenGL API controller
pub struct OpenGL {
    /// The target window
    pub window: GLWindow,
}

impl OpenGL {
    /// Create a new OpenGL API controller
    pub fn new(_app_context: AppCreateInfo, win_opts: WindowOptions) -> (Self, EventLoop<()>) {
        let (window, evenloop) = GLWindow::new(win_opts);

        implement_vertex!(Vertex, position, tex_cords);

        (
            Self {
                window,
            },
            evenloop
        )
    }

    /// Draws stuff on the screen. Run this every time you want to update the screen content.
    fn draw_frame(display: Display, objects_manager: &ObjectManager) {

        // drawing a frame
        let mut target_frame = display.draw();
        target_frame.clear_color(0.0, 0.0, 0.0, 0.0);

        let objects = objects_manager.get_objects();
        for object in objects {
            let vertex_buffer = {
                glium::VertexBuffer::new(&display,
                                         &object.vertices
                ).unwrap()
            };

            
            let shader = &object.shader_program;
            let program = shader.opengl_get_shader(&display).unwrap();

            // building the uniforms
            let _uniform_arguments = shader.opengl_get_uniform_arguments();
            let uniform = uniform! {
                matrix: objects_manager.get_view_matrix()
            };

            // building the index buffer
            let index_buffer = glium::IndexBuffer::new(&display,
                                                       PrimitiveType::TrianglesList,
                                                       &object.indices).unwrap();

            
            target_frame.draw(&vertex_buffer, &index_buffer, &program, &uniform, &Default::default()).unwrap();
        }

        target_frame.finish().unwrap();
    }
}

impl Renderer for OpenGL {
    /// Init OpenGL API
    fn init(&mut self) { }

    /// Run this if the window has been resized
    fn resize(&mut self) { }

    /// Run this if you want to display the next frame
    fn next_frame(&mut self, render_objects: &ObjectManager) {
        OpenGL::draw_frame(self.window.display.clone(), render_objects);
    }

    /// Cleans up the OpenGL memory. Run this before you quite the program.
    fn cleanup(&mut self) {
        debug!("Starting to clean up");
    }

    /// Gets the window dimensions
    fn get_dimensions(&mut self) -> (u32, u32) {
        (0, 0) //TODO get the dimensions
    }

    /// Gets which API this is
    fn get_api(&mut self) -> String {
        "OpenGL".parse().unwrap()
    }
}
