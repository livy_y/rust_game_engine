use glium::Display;
use glium::Program;

use crate::utils::color::{Color, color_to_array};

pub enum UniformArguments {
    ViewMatrix,
    Color([f32; 3]),
    Vec3([f32; 3]),
    Vec4([f32; 4]),
}

/// # Shader Trait
/// This is a basic shader trait so that you can create your
/// own shader programs. Look at the SimpleShader for an 
/// example.
pub trait Shader {
    fn get_version(&self) -> u32;
    fn get_vertex_shader(&self) -> String;
    fn get_fragment_shader(&self) -> String;
    #[cfg(feature = "opengl")]
    fn opengl_get_uniform_arguments(&self) -> Vec<UniformArguments>;
    #[cfg(feature = "opengl")]
    fn opengl_get_shader(&self, display: &Display) -> Result<Program, glium::program::ProgramChooserCreationError>;
}


/// # Simple Shader
/// This is the most basic shader version that only supports 
/// glsl version 420.
///
/// You only need to imput the vertex and fragment shader. The
/// shaderprogram will do the rest.
pub struct SimpleShader {
    vertex_shader: String,
    fragment_shader: String,
    uniform_aguments: Vec<UniformArguments>,
}

impl SimpleShader {
    pub fn new(vertex_shader: String, fragment_shader: String, uniform_aguments: Vec<UniformArguments>) -> Self {
        Self { vertex_shader, fragment_shader, uniform_aguments}
    }
}

impl Shader for SimpleShader {
    fn get_version(&self) -> u32 { 420 }

    fn get_vertex_shader(&self) -> String {
        self.vertex_shader.clone()
    }

    fn get_fragment_shader(&self) -> String {
        self.fragment_shader.clone()
    }

    
    fn opengl_get_uniform_arguments(&self) -> Vec<UniformArguments> {
            vec![UniformArguments::ViewMatrix]
        }

    fn opengl_get_shader(&self, display: &Display) 
            -> Result<Program, glium::program::ProgramChooserCreationError> {
        program!(display, 420 => 
                 {vertex: &self.vertex_shader, fragment: &self.fragment_shader})
    }
}

/// # Solid Color Shader
/// This is a very simple shader that outputs a certain
/// color.
/// You just need to input the color, the shader program
/// will do the rest.
///
/// If you  would like to see the shader code, then 
/// look into the source code of this struct.
pub struct SolidColorShader {
    color: Color,
}

impl SolidColorShader {
    pub fn new(color: Color) -> Self {
        Self { color }
    }

    pub fn default() -> Self {
        Self { color: Color::Blue }
    }

    fn color_to_string(&self) -> String {
        let array = color_to_array(self.color);

        let mut result: String = "".to_string();

        for x in array {
            result.push_str(x.to_string().as_str());
            result.push_str(", ");
        }

        let result: String = (&result[0..result.len() - 2]).to_string();
        println!("{}", result);
        return result

    }
}
impl Shader for SolidColorShader {
    fn get_version(&self) -> u32 {
        420
    }

    fn get_vertex_shader(&self) -> String {
        format!("
        #version 140
        uniform mat4 matrix;

        in vec3 position;

        out vec3 vColor;

        void main() {{
            gl_Position = vec4(position, 1.0) * matrix;
            vColor = vec3({});
        }}
        ", self.color_to_string())
    }

    fn get_fragment_shader(&self) -> String {
        r#"
        #version 140
        in vec3 vColor;
        out vec4 f_color;

        void main() {
            f_color = vec4(vColor, 1.0);
        }
        "#
        .parse()
        .unwrap()
    }

    fn opengl_get_uniform_arguments(&self) -> Vec<UniformArguments> {
        vec![UniformArguments::ViewMatrix, UniformArguments::Color(color_to_array(self.color))]
    }

    fn opengl_get_shader(&self, display: &Display) 
            -> Result<Program, glium::program::ProgramChooserCreationError> {
        program!(display, 140 => 
                 {vertex: &self.get_vertex_shader(), fragment: &self.get_fragment_shader()})
    }

}

