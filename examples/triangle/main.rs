use cgmath::{Angle, Deg, Vector3, Point3};
use log::{debug, info};
use tulpen::{AppCreateInfo, engine, graphics::renderer::RenderAPI, Version};
use tulpen::graphics::renderer::{Renderer, Vertex};
use tulpen::graphics::window::WindowOptions;
use tulpen::scene::{RenderObject, ObjectManager};
use tulpen::scene::shapes::ShapeBuilder;
use tulpen::scene::shader::SolidColorShader;

struct Game {
    rotation: u64,
}

impl engine::App for Game {
    fn create(&mut self) { debug!("created the game") }

    fn update(&mut self, _renderer: &mut Box<dyn Renderer>, objs: &mut ObjectManager, _delta: i128) {

        let triangle = ShapeBuilder::new()
            .height(0.5)
            .width(0.3)
            .color([1.0, 1.0, 1.0])
            .position(Point3::new(0.5, 0.5, 0.5))
            .build();
        objs.objects.push(triangle);

        // Draw a vertical line in the middle of the screen
        let v_line = RenderObject{
            vertices: Vec::from([
                Vertex { position: [ 5.0,  0.000, 1.], tex_cords: [0., 0.]},
                Vertex { position: [-5.0, -0.004, 1.], tex_cords: [0., 0.]},
                Vertex { position: [-5.0,  0.004, 1.], tex_cords: [0., 0.]},
            ]),
            indices: vec![1,2,3],
            shader_program: Box::new(SolidColorShader::default()),
        };
        objs.objects.push(v_line);


        // Draw a horizontale line in the middle of the screen
        let h_line = RenderObject{
            vertices: Vec::from([
                Vertex { position: [ 50.0,  50.0, -10.], tex_cords: [0., 0.]},
                Vertex { position: [-50.0, -50.0, -10.], tex_cords: [0., 0.]},
                Vertex { position: [ 10.0, -50.0, -10.], tex_cords: [0., 0.]},
            ]),
            indices: vec![1,2,3],
            shader_program: Box::new(SolidColorShader::default()),
        };
        objs.objects.push(h_line);

        // Draw a vertical line in the middle of the screen
        let v_line = RenderObject{
            vertices: Vec::from([
                Vertex { position: [ 5.0,  0.000, 1.], tex_cords: [0., 0.]},
                Vertex { position: [-5.0, -0.004, 1.], tex_cords: [0., 0.]},
                Vertex { position: [-5.0,  0.004, 1.], tex_cords: [0., 0.]},
            ]),
            indices: vec![1,2,3],
            shader_program: Box::new(SolidColorShader::default()),
        };
        objs.objects.push(v_line);

        objs.camera.move_camera(1., 1., 1.);
    }

    fn resize(&mut self, _width: u32, _height: u32) { debug!("resized the window") }

    fn suspend(&mut self) {
        debug!("suspend the game")
    }

    fn resumed(&mut self) {
        debug!("resume the game")
    }

    fn quit(&mut self) {
        debug!("quit the game")
    }
}

impl Game {
    pub fn new() -> Game {
        Game{ rotation: 0}
    }
}

fn main() {
    info!("Starting game!");

    let window_options = WindowOptions::default();

    let creation_info = AppCreateInfo {
        app_name: "Triangle game".parse().unwrap(),
        app_version: Version::default(),
        graphics_api: RenderAPI::OpenGL,
        log_config: None, 
    };

    let game = Game::new();
    engine::start_engine(game, creation_info, window_options);

    info!("Quited game!");
}
